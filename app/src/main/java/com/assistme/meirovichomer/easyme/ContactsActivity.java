package com.assistme.meirovichomer.easyme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class ContactsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        //AdMob
        //Initialize ADS
        adMobIntilize();

        //Functions
        listOfContacts();
        keepScreenAwake();
        endActivityEvent();
        addContactEvent();
        deleteContactEvent();


    }


    // On endActivity click ends the activity
    protected void endActivityEvent() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToHomeBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    protected void addContactEvent() {
        Button toContacts = (Button) findViewById(R.id.addContactBtn);
        toContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactsActivity.this, AddContact.class));
            }
        });
    }


    protected void deleteContactEvent() {
        Button toDeleteContacts = (Button) findViewById(R.id.deleteContactBtn);
        toDeleteContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactsActivity.this, DeleteContact.class));
            }
        });

    }


    protected void listOfContacts() {
        Button toDeleteContacts = (Button) findViewById(R.id.listOfContactsBtn);
        toDeleteContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactsActivity.this, ContactsList.class));
            }
        });

    }


    // Function keeps screen awake so it wont go dark.
    protected void keepScreenAwake(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Function initializing ad.
    protected void adMobIntilize(){

        AdView mAdView;

        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/5709245544");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }



}


