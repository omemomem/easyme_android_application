package com.assistme.meirovichomer.easyme;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.provider.ContactsContract;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import classes.SelectUserAdapterCall;
import classes.SelectUserAdapterDelete;
import classes.SelectUserCall;
import classes.SelectUserDelete;

public class DeleteContact extends Activity {


    // ArrayList
    ArrayList<SelectUserDelete> selectUserDels;
    List<SelectUserDelete> temp;
    // Contact List
    ListView listView;
    // Cursor to load contacts list
    Cursor phones;

    // Pop up
    ContentResolver resolver;
    SearchView search;
    SelectUserAdapterDelete adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_contact);
        keepScreenAwake();
        endActivityEvent();

        selectUserDels = new ArrayList<SelectUserDelete>();
        resolver = this.getContentResolver();
        listView = (ListView) findViewById(R.id.contacts_list);

        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        LoadContact loadContact = new LoadContact();
        loadContact.execute();

        search = (SearchView) findViewById(R.id.searchView);

        //*** setOnQueryTextListener ***
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub
                adapter.filter(newText);
                return false;
            }
        });
    }

    // Load data on background
    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        // InBackground will append all the contacts into the SelectUserCall which later on will append it onto the listView
        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            selectUserDels = new ArrayList<SelectUserDelete>();

            if (phones != null) {

                if (phones.getCount() == 0) {
                    Toast.makeText(DeleteContact.this, "No contacts in your contact list.", Toast.LENGTH_LONG).show();
                }


                Integer[] ids = new Integer[phones.getCount() * 2];
                ids[0] = 1;
                String[] numbers = new String[phones.getCount() * 2];
                ids[0] = 1;
                Integer i = 1;
                while (phones.moveToNext()) {


                    // Gets he id, number and display name from the phone data using the phones courser.
                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    phoneNumber = phoneNumber.replaceAll("-", "");
                    phoneNumber = phoneNumber.replaceAll(" ", "");
                    // input = input.replace(" ", "");

                    ids[i] = Integer.parseInt(id);
                    numbers[i] = phoneNumber;

                    if (!ids[i].equals(ids[i - 1]) && !numbers[i].equals(numbers[i-1])) {

                        //SelectUserCall class will save objects with the id,name and phonenumber and later on will add it to listView
                        SelectUserDelete selectuserdelete = new SelectUserDelete();
                        selectuserdelete.setName(name);
                        selectuserdelete.setPhone(phoneNumber);
                        selectuserdelete.setEmail(id);
                        selectUserDels.add(selectuserdelete);
                    }
                    i++;
                }
            } else {

            }
            // phones.close();
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            // Use this so you can see ALL of contacts even if DUPLICATE.
            adapter = new SelectUserAdapterDelete(selectUserDels, DeleteContact.this);
            listView.setAdapter(adapter);


            listView.setAdapter(adapter);


            // Select item on listclick.
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override

                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    SelectUserDelete data = selectUserDels.get(i);
                    String userPhoneNumber = data.getPhone();
                    String userContactName = data.getName();

                    deleteContFunction(resolver, userPhoneNumber);
                    Toast.makeText(DeleteContact.this, "איש קשר נמחק", Toast.LENGTH_LONG).show();
                    toNoticeUser(userContactName);
                    //close activity
                    finish();
                    // Dialog opens to check if user is sure about delete.
//                    areYouSureDialog(data, resolver);

                }
            });

            listView.setFastScrollEnabled(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        phones.close();
    }

    //  unction finishes current activity.
    protected void endActivityEvent() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToContactBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Function deletes a contact.
    public static void deleteContFunction(ContentResolver contactHelper, String
            number) {
        ArrayList<ContentProviderOperation> ops = new
                ArrayList<ContentProviderOperation>();
        String[] args = new String[]{String.valueOf(getContactID(contactHelper,
                number))};
        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI).withSelection(ContactsContract.RawContacts.CONTACT_ID + "=?", args).build());
        try {
            contactHelper.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }


    // Function used in the DeleteContactFunction, get the id of the number chosen from the DeleteList.
    private static long getContactID(ContentResolver contactHelper, String
            number) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] projection = {ContactsContract.PhoneLookup._ID};
        Cursor cursor = null;
        try {
            cursor = contactHelper.query(contactUri, projection, null, null, null);
            if (cursor.moveToFirst()) {
                int personID = cursor.getColumnIndex(ContactsContract.PhoneLookup._ID);
                return cursor.getLong(personID);
            }
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return -1;
    }

    // A dilaog that will open when a contact is clicked, verify that you want to delete the contact.
    protected void areYouSureDialog(final SelectUserDelete tmpUser, final ContentResolver resTmp) {

        final String userPhoneNumber = tmpUser.getPhone();
        final String userContactName = tmpUser.getName();

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        //new AlertDialog.Builder(this)

        dialog.setTitle("מחק איש קשר");
        dialog.setMessage("האם למחוק את: " + userContactName + " מהרשימה?");
        dialog.setPositiveButton("מחק", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setNegativeButton("לא למחוק", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing.
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();

    }


    //Keeps the screen always on, disables the screen sleep.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    //When user deletes a contact will send him to the NoticeActivity also adding sharedprefs to design the notice page.
    protected void toNoticeUser(String nameOfPerson) {


        String PREFS_NAME = "notice_page";

        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("noticeMode", "deleteContact");
        editor.commit();

        Intent myIntent = new Intent(DeleteContact.this, NoticeActivity.class);
        myIntent.putExtra("firstName", nameOfPerson);
        startActivity(myIntent);
    }


}


