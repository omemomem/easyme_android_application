package com.assistme.meirovichomer.easyme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FirstRunConfig extends AppCompatActivity {

    public static final String PREFS_SCREEN = "screen_config";

    SharedPreferences screenFunction = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_run_config);

        screenFunction = getSharedPreferences(PREFS_SCREEN, MODE_PRIVATE);

        confirmOnClick();
        denyOnClick();


    }

    protected void confirmOnClick() {

        Button confirm = (Button) findViewById(R.id.acceptConfig);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //true config
                screenFunction.edit().putString(PREFS_SCREEN, "true").apply();
                startActivity(new Intent(FirstRunConfig.this, MainActivity.class));
                finish();
            }
        });


    }


    protected void denyOnClick() {

        Button confirm = (Button) findViewById(R.id.denyConfig);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //true config
                screenFunction.edit().putString(PREFS_SCREEN, "false").apply();
                startActivity(new Intent(FirstRunConfig.this, MainActivity.class));
                finish();
            }
        });


    }


}
