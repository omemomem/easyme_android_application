package com.assistme.meirovichomer.easyme;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.w3c.dom.Text;


public class NoticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        adMobIntilize();
        //Functions
        keepScreenAwake();
        setStyleOnOpen();
        finishActivityNotice();
        onMoovitDownloadClick();
    }


    // sets the background color, textviews text and etc...
    protected void setStyleOnOpen() {

        String PREFS_NAME = "notice_page";

        SharedPreferences prefs = this.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String modeName = prefs.getString("noticeMode", null);


        TextView noticeText = (TextView) findViewById(R.id.noticeTextView);
        Button confirmBtn = (Button) findViewById(R.id.ConfirmNoticeButton);
        Button moovitBtn = (Button) findViewById(R.id.MoovitDownloadBtn);
        Button moovitDoNotBtn = (Button) findViewById(R.id.refuseDownloadMoovitBtn);
        ConstraintLayout noticeLayout = (ConstraintLayout)findViewById(R.id.noticeLayout);


        if (modeName.equals("moovitDownloadActivity")) {
            noticeText.setText("עלייך להתקין את האפליקצה מוביט בכדי לחפש אוטובוסים.");
            noticeText.setTextColor(Color.parseColor("#F0EAD8"));
            noticeLayout.setBackgroundColor(Color.parseColor("#407577"));
            moovitBtn.setVisibility(View.VISIBLE);
            confirmBtn.setVisibility(View.INVISIBLE);
            moovitDoNotBtn.setVisibility(View.VISIBLE);


            prefs.edit().remove(PREFS_NAME).apply();

        } else if (modeName.equals("addContact")) {
            noticeText.setText("איש הקשר הוסף בהצלחה! לחץ אישור להמשך");
            noticeText.setTextColor(Color.parseColor("#F0EAD8"));
            noticeLayout.setBackgroundColor(Color.parseColor("#407577"));
            prefs.edit().remove(PREFS_NAME).apply();
        } else if (modeName.equals("deleteContact")) {

            Intent intent = getIntent();

            String nameDeleted = intent.getStringExtra("firstName");
            String htmlString=  "איש הקשר: " + "<u>"+ nameDeleted +"</u>" + " נמחק בהצלחה! לחץ אישור להמשך.";
            noticeText.setText(Html.fromHtml(htmlString));
            noticeText.setTextColor(Color.parseColor("#F0EAD8"));
            noticeLayout.setBackgroundColor(Color.parseColor("#407577"));
            prefs.edit().remove(PREFS_NAME).apply();
        } else {
            prefs.edit().remove(PREFS_NAME).apply();
        }

    }

    // if user selects that he is not intrested in downloading moovit or he confirmed any other notices, Finish.
    protected void finishActivityNotice() {

        Button confirmBtn = (Button) findViewById(R.id.ConfirmNoticeButton);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button moovitDoNot = (Button) findViewById(R.id.refuseDownloadMoovitBtn);
        moovitDoNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // finishes the activity and starts the openMoovitDownloadClick();
    protected void onMoovitDownloadClick() {
        Button moovitBtn = (Button) findViewById(R.id.MoovitDownloadBtn);
        moovitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMoovitDownloadPage();
                finish();
            }
        });


    }


    protected void adMobIntilize() {

        AdView mAdView;


        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/1921834349");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    // Keeps screen awake , Screen sleep is off.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    // Sends user to store to the moovit page in order to download it.
    public void openMoovitDownloadPage() {
        // Moovit not installed - send to store
        String url = "http://app.appsflyer.com/com.tranzmate?pid=DL&c=<AssistMe>";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
        finish();
    }
}
