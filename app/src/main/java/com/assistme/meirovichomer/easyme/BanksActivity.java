package com.assistme.meirovichomer.easyme;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class BanksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banks);
        //AdMob
        adMobIntilize();
        //Function
        keepScreenAwake();
        //ToBanks function
        toLeumi();
        toHapoalim();
        toDiscount();
        toMizrahi();
        toBenLeumi();

        endActivity();
    }



    // Function initializing an ad.
    protected void adMobIntilize(){

        AdView mAdView;


        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/1139445149");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public void toLeumi() {

        Button toLeumi = (Button) findViewById(R.id.toLeumiWebBtn);
        toLeumi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://hb2.bankleumi.co.il/uniquesig4e0824291ffbe1b42058d6558ed87217/uniquesig0/InternalSite/CustomUpdate/eBank_ULI_Login.asp?resource_id=C66B095BD60649D18ECB79F04C657517&login_type=2&site_name=leumi&secure=1&URLHASH=e7506107-0db6-457a-b982-ad2af80f0205&orig_url=https%3a%2f%2fhb2.bankleumi.co.il%2fH%2fLogin.html"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);


            }
        });
    }

    public void toHapoalim() {

        Button toPoailm = (Button) findViewById(R.id.toHapoalimBtn);
        toPoailm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://login.bankhapoalim.co.il/cgi-bin/poalwwwc?reqName=getLogonPagePoalim"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);


            }
        });
    }

    public void toDiscount() {

        Button toPoailm = (Button) findViewById(R.id.toDiscountBtn);
        toPoailm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.discountbank.co.il/DB/private"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);


            }
        });
    }

    public void toMizrahi() {

        Button toPoailm = (Button) findViewById(R.id.toMizrahiBtn);
        toPoailm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.mizrahi-tefahot.co.il/login/LoginMTO.aspx?errorcode=198&TYPE=100663297&REALMOID=06-379e9a46-0ea8-4ab6-8ab1-880e6888203e&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=$SM$e7C0GBhQlQIsjzKU1bTHYzA0SjkxVktpnumjMG0JJY5aUBhHzLoziO%2fUD5Ind0FI&TARGET=$SM$HTTP%3a%2f%2fmto%2emizrahi-tefahot%2eco%2eil%2fOnline%2fDefault%2easpx%3flanguage%3dhe-IL%26page%3d~%2fIC%2fP946%2easpx"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });
    }


    public void toBenLeumi() {

        Button toPoailm = (Button) findViewById(R.id.toBenLeumiBtn);
        toPoailm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://online.fibi.co.il/wps/portal"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });
    }

    // Button event to exit from the app.
    protected void endActivity() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToHomeBtnFromBanks);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    // Function keeps screen awake without letting it sleep (go dark).
    protected void keepScreenAwake(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


}
