package com.assistme.meirovichomer.easyme;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class HealthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health);
        //adMob
        adMobIntilize();
        //Functions
        keepScreenAwake();
        // To the websites functions.
        toKlalit();
        toMaccabi();
        toLeumit();
        toMeuhedet();
        //Finish Activity
        endActivity();
    }

    protected void adMobIntilize(){

        AdView mAdView;

        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/2616178348");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public void toKlalit() {

        Button toKlal = (Button) findViewById(R.id.toKlalitWebsiteBtn);
        toKlal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://e-services.clalit.org.il/onlineweb/general/login.aspx"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

    }


    public void toLeumit() {

        Button toLeumi = (Button) findViewById(R.id.toLeumitWebsiteBtn);
        toLeumi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://online2.leumit.co.il/online/login/login.aspx"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }


    public void toMeuhedet() {

        Button toMeuhed = (Button) findViewById(R.id.toMeuhedetWebsiteBtn);
        toMeuhed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(getString(R.string.linkToMeuhedet)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }


    public void toMaccabi() {

        Button toMaca = (Button) findViewById(R.id.toMaccabiWebsiteBtn);
        toMaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://online.maccabi4u.co.il/dana-na/auth/url_44/welcome.cgi"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    // Button event to exit from the app.
    protected void endActivity() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToHomeBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void keepScreenAwake(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

}
