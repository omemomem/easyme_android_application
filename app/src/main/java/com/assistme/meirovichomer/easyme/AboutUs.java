package com.assistme.meirovichomer.easyme;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class AboutUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        toPrivacyPolicy();
        adMobIntilize();
        endActivity();
        keepScreenAwake();
        contactUs();
    }


    protected void toPrivacyPolicy(){

        Button policy = (Button)findViewById(R.id.toPrivacyPolicy);
        policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://docs.google.com/document/d/1MGhgtcBHmRrN6-gMzlCzFPxmwCASqyB1auYZewxktLg/pub"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }


    protected void adMobIntilize() {

        AdView mAdView;

        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/1921834349");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    // Button event to exit from the app.
    protected void endActivity() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.closeApp_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Button event to exit from the app.
    protected void contactUs() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.contactUs);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailStart();
            }
        });
    }

    // Keep screen awake, meaning screen sleep is off.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }



    protected void emailStart(){

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"EasyMeApplication@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "כתוב את שמך המלא");
        email.putExtra(Intent.EXTRA_TEXT, "");
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }

}
