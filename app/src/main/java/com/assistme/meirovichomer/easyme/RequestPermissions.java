package com.assistme.meirovichomer.easyme;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RequestPermissions extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permissions);
        keepScreenAwake();
        buttonsListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();
        permissionsConfirmationButtonsOn();

    }


    protected void buttonsListeners() {
        Button btnCall = (Button) findViewById(R.id.CallConfirm);
        Button btnLoc = (Button) findViewById(R.id.LocConfirm);
        Button btnCont = (Button) findViewById(R.id.contactsConfirm);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPermission();
            }
        });
        btnLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locPermission();
            }
        });
        btnCont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readPermission();
            }
        });

    }


    protected void permissionsConfirmationButtonsOn() {

        Button btnContact = (Button) findViewById(R.id.contactsConfirm);
        Button btnLocation = (Button) findViewById(R.id.LocConfirm);
        Button btnCall = (Button) findViewById(R.id.CallConfirm);


        if (confirmRead()) {
            btnContact.setVisibility(View.INVISIBLE);
        }
        if (confirmFineLoc()) {
            btnLocation.setVisibility(View.INVISIBLE);
        }
        if (confirmCallPhone()) {
            btnCall.setVisibility(View.INVISIBLE);
        }

        if (confirmCallPhone() && confirmFineLoc() && confirmRead()){
            finish();
        }

    }


    protected Boolean confirmRead() {
        int writeContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);

        return writeContacts == PackageManager.PERMISSION_GRANTED;
    }

    protected void readPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 101);
    }

    protected Boolean confirmFineLoc() {
        int fineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        return fineLocation == PackageManager.PERMISSION_GRANTED;
    }

    protected void locPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 102);

    }

    protected Boolean confirmCallPhone() {

        int callPhone = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        return callPhone == PackageManager.PERMISSION_GRANTED;
    }

    protected void callPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 103);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case 101:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finishActivity();
                    startActivity(new Intent(RequestPermissions.this, MainActivity.class));
                }
                return;
            case 102:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    finishActivity();
                    startActivity(new Intent(RequestPermissions.this, MainActivity.class));

                }
                return;
            case 103:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finishActivity();
                    startActivity(new Intent(RequestPermissions.this, MainActivity.class));

                }
                return;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    // Keeps screen awake, screen sleep is off,
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    protected void finishActivity() {
        finish();
    }


}
