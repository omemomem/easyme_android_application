package com.assistme.meirovichomer.easyme;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import classes.SelectUserAdapterCall;
import classes.SelectUserCall;
import classes.ViewDialog;

public class ContactsList extends AppCompatActivity {



    // ArrayList
    ArrayList<SelectUserCall> selectUserCalls;
    List<SelectUserCall> temp;
    // Contact List
    ListView listView;
    // Cursor to load contacts list
    Cursor phones;

    // Pop up
    ContentResolver resolver;
    SearchView search;
    SelectUserAdapterCall adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);
        keepScreenAwake();
        endActivityEvent();

        selectUserCalls = new ArrayList<SelectUserCall>();
        resolver = this.getContentResolver();
        listView = (ListView) findViewById(R.id.contactsListCall);

        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        ContactsList.LoadContact loadContact = new ContactsList.LoadContact();
        loadContact.execute();

        search = (SearchView) findViewById(R.id.searchViewCall);

        //*** setOnQueryTextListener ***
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub
                adapter.filter(newText);
                return false;
            }
        });

    }



    // Load data on background
    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        // InBackground will append all the contacts into the SelectUserCall which later on will append it onto the listView
        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            selectUserCalls = new ArrayList<SelectUserCall>();

            if (phones != null) {

                if (phones.getCount() == 0) {
                    Toast.makeText(ContactsList.this, "No contacts in your contact list.", Toast.LENGTH_LONG).show();
                }


                Integer[] ids = new Integer[phones.getCount() * 2];
                ids[0] = 1;
                String[] numbers = new String[phones.getCount() * 2];
                ids[0] = 1;
                Integer i = 1;
                while (phones.moveToNext()) {

                    // Gets he id, number and display name from the phone data using the phones courser.
                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    phoneNumber = phoneNumber.replaceAll("-", "");
                    phoneNumber = phoneNumber.replaceAll(" ", "");
                    // input = input.replace(" ", "");

                    ids[i] = Integer.parseInt(id);
                    numbers[i] = phoneNumber;

                    if (!ids[i].equals(ids[i - 1]) && !numbers[i].equals(numbers[i-1])) {

                        //SelectUserCall class will save objects with the id,name and phonenumber and later on will add it to listView
                        SelectUserCall selectUserCall = new SelectUserCall();
                        selectUserCall.setName(name);
                        selectUserCall.setPhone(phoneNumber);
                        selectUserCall.setEmail(id);
                        selectUserCalls.add(selectUserCall);
                    }
                    i++;

                }
            } else {

            }
            // phones.close();
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            // Use this so you can see ALL of contacts even if DUPLICATE.
            adapter = new SelectUserAdapterCall(selectUserCalls, ContactsList.this);
            listView.setAdapter(adapter);

            // Select item on listclick.
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override

                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    SelectUserCall data = selectUserCalls.get(i);
                    // Get the contact that was clicked phone number.
                    String userPhoneNumber = data.getPhone();
                    String userName = data.getName();
                    openDialog(userPhoneNumber,userName);


                }
            });

            listView.setFastScrollEnabled(true);
        }
    }

    protected void openDialog(String phone , String name){

        ViewDialog alert = new ViewDialog();
        alert.showDialog(ContactsList.this,phone,name);


    }

    //When activity is stopped. phones courser is closed as stopped as well.
    @Override
    protected void onStop() {
        super.onStop();
        phones.close();
    }

    protected void endActivityEvent() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToHomeBtnFromCalls);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    //Keeps the screen always on, disables the screen sleep.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


}
