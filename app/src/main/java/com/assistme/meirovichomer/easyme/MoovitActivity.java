package com.assistme.meirovichomer.easyme;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import classes.CitiesString;


public class MoovitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moovit);

        keyBoardOnHideBtns();
        //adMob
        adMobIntilize();
        //Functions
        keepScreenAwake();
        onEditTextsClick();
        checkIfMoovitExists();
        onMoovitBtnClick();
        autoCompleteCity();
        backToMain();

    }


    // Function checks if keyoard is on, If so hides the bottom buttons and the adView
    protected void keyBoardOnHideBtns() {


        final View contentView = findViewById(android.R.id.content);


        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Button backButton = (Button) findViewById(R.id.backToHomebtnFromMoovit);
                AdView adView = (AdView) findViewById(R.id.adView);

                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("CheckHeight", "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    backButton.setVisibility(View.GONE);
                    adView.setVisibility(View.GONE);
                } else {
                    // keyboard is closed
                    backButton.setVisibility(View.VISIBLE);
                    adView.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    protected void adMobIntilize() {

        AdView mAdView;


        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        // MOOVIT BANNER AD : ca-app-pub-2474037887194071/8662711944
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/8662711944");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public void onMoovitBtnClick() {

        Button btn = (Button) findViewById(R.id.toMoovitAppBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                geoLocationWhenPressed();
            }
        });


    }


    // This activity opens the moovit app, will be used when button clicked and search was done.
    public void startMoovit(Double startLat, Double startLon, Double destLat, Double destLon, String startPlace, String destPlace) {

        // ALL THE PARAMETERS ABOVE -> those are the coordinates of the address the user chose, will be sent to the route activity in Moovit.
        Context context = this;
        try {
            // Assume that Moovit app exists. If not, exception will occur
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.tranzmate", PackageManager.GET_ACTIVITIES);
            String uri =
                    // Launch app call - scheme (with parameters)
                    "moovit://directions?dest_lat=" + destLat + "&dest_lon=" + destLon + "&dest_name=" + destPlace + "&orig_lat=" + startLat + "&orig_lon=" + startLon + "&orig_name=" + startPlace + "&partner_id=<AssistMe>";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(uri));
            startActivity(intent);

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(context, "An error has occurred,Moovit not installed. ", Toast.LENGTH_SHORT).show();
        }
    }

    //Check if moovit app is installed on users device.
    public void checkIfMoovitExists() {

        Context context = this;
        try {
            // Assume that Moovit app exists. If not, exception will occur
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.tranzmate", PackageManager.GET_ACTIVITIES);

        } catch (PackageManager.NameNotFoundException e) {
            toNoticeUser();
            finish();
        }
    }


    // Auto complete the cities using cities.java file. (the cites.java contains all cities in israel)
    public void autoCompleteCity() {


        // 1st textView. START POINT.
        CitiesString cities = new CitiesString();
        ArrayAdapter<String> adapterStart = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, cities.getString()
        );

        AutoCompleteTextView cityStart = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteCityStart);
        adapterStart.setNotifyOnChange(true);
        cityStart.setThreshold(2);
        cityStart.setAdapter(adapterStart);

        // 2nd textView. DESTINATION.
        ArrayAdapter<String> adapterDest = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, cities.getString());

        AutoCompleteTextView cityDest = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityDest);
        adapterStart.setNotifyOnChange(true);
        cityDest.setThreshold(2);
        cityDest.setAdapter(adapterDest);

    }

    public void autoCompleteStreet() {

        String obj = loadJSONFromAsset();

        JSONObject issueObj;
        try {
            issueObj = new JSONObject(obj);
            Iterator iterator = issueObj.keys();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                JSONObject issue = issueObj.getJSONObject(key);

                //  get id from  issue
                String _pubKey = issue.optString("id");
                Log.e("key: " , _pubKey);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    public String loadJSONFromAsset() {
        String json;
        try {

            // BUG: File not found , tried different methods and added assets file, also changed name etc...
            InputStream is = getAssets().open("MoovitCityStreet.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

            // json = new JSONObject(buffer.toString());


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    // Takes the values in the autocomplete.. and search in Geocoder, afterwards sends the data to StartMoovit();
    public void geoLocationWhenPressed() {

        AutoCompleteTextView cityDest = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityDest);
        AutoCompleteTextView streetDest = (AutoCompleteTextView) findViewById(R.id.streetNameDest);
        AutoCompleteTextView cityStart = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityStart);
        AutoCompleteTextView streetStart = (AutoCompleteTextView) findViewById(R.id.streetNameStart);

        if (cityDest.getText().length() > 0 && streetDest.getText().length() > 0 && cityStart.getText().length() > 0 && streetStart.getText().length() > 0) {

            String startPoint = cityStart.getText().toString() + " " + streetStart.getText().toString();
            String destPoint = cityDest.getText().toString() + " " + streetDest.getText().toString();

            Geocoder searchGeo = new Geocoder(this, Locale.getDefault());

            List<Address> listStart, listDest;

            try {

                listStart = searchGeo.getFromLocationName(startPoint, 1);
                listDest = searchGeo.getFromLocationName(destPoint, 1);

                Double latStart = listStart.get(0).getLatitude();
                Double lonStart = listStart.get(0).getLongitude();

                Double latDest = listDest.get(0).getLatitude();
                Double lonDest = listDest.get(0).getLongitude();
                //Only if search results inside the borders of israel will let the user enter moovit app.
                if (checkInsideBorders(latStart, lonStart, latDest, lonDest)) {
                    startMoovit(latStart, lonStart, latDest, lonDest, startPoint, destPoint);
                } else {

                    Toast.makeText(this, "אנא וודא כי הקלדת את הכתובת כראוי", Toast.LENGTH_SHORT).show();

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ifAutosAreEmpty();
        }
    }


    // Function to check if the results will be inside the borders of israel
    public boolean checkInsideBorders(Double latStart, Double lonStart, Double latDest, Double lonDest) {

        //Created 'BORDERS' search for the moovit so only search results inside the square (between the numbers below) will send you to
        // the page.
        if (latStart > 29.313225 && latDest > 29.313225 && latStart < 33.530406 && latDest < 33.530406 &&
                lonStart > 33.954786 && lonDest > 33.954786 && lonStart < 36.042188 && lonDest < 36.042188) {
            return true;
        }
        return false;


    }

    // In case not all fields are filled color the hintText in RED.
    public void ifAutosAreEmpty() {


        AutoCompleteTextView cityDest = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityDest);
        AutoCompleteTextView streetDest = (AutoCompleteTextView) findViewById(R.id.streetNameDest);
        AutoCompleteTextView cityStart = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityStart);
        AutoCompleteTextView streetStart = (AutoCompleteTextView) findViewById(R.id.streetNameStart);


        Toast.makeText(this, "אנא וודא כי כל השדות מלאים", Toast.LENGTH_LONG).show();


        cityDest.setHintTextColor((Color.parseColor("#F10707")));

        streetDest.setHintTextColor((Color.parseColor("#F10707")));

        cityStart.setHintTextColor((Color.parseColor("#F10707")));

        streetStart.setHintTextColor((Color.parseColor("#F10707")));

    }

    //Keeps screen awake, Screen sleep is off.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    //In case moovit is not installed, sends user to NoticeActivity to ask him if he want to download moovit, sets sharedprefs to design notice.
    protected void toNoticeUser() {

        String PREFS_NAME = "notice_page";

        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("noticeMode", "moovitDownloadActivity");
        editor.commit();
        startNoticeAtivity();

    }

    // Notice acitivity in case moovit is not installed.
    protected void startNoticeAtivity() {

        startActivity(new Intent(MoovitActivity.this, NoticeActivity.class));

    }

    protected void backToMain() {

        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.backToHomebtnFromMoovit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    // When an editText is clicked makes the background of that editText darker.
    protected void onEditTextsClick() {


        final AutoCompleteTextView cityDest = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityDest);
        final AutoCompleteTextView streetDest = (AutoCompleteTextView) findViewById(R.id.streetNameDest);
        final AutoCompleteTextView cityStart = (AutoCompleteTextView) findViewById(R.id.autoCompleteCityStart);
        final AutoCompleteTextView streetStart = (AutoCompleteTextView) findViewById(R.id.streetNameStart);


        // cityDest text onClick
        cityDest.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {


                // REGULAR: FCF7E6   DARKER: CEC29B
                cityDest.setBackgroundColor(Color.parseColor("#CEC29B"));
                streetDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                cityStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                streetStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }

        });

        // streetDest  onClick
        streetDest.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                // REGULAR: FCF7E6   DARKER: CEC29B
                streetDest.setBackgroundColor(Color.parseColor("#CEC29B"));
                cityDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                cityStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                streetStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }

        });


        // cityStart onClick
        cityStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                // REGULAR: FCF7E6   DARKER: CEC29B
                cityStart.setBackgroundColor(Color.parseColor("#CEC29B"));
                cityDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                streetDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                streetStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }
        });

        // Phone number onClick
        streetStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                // REGULAR: FCF7E6   DARKER: CEC29B
                streetStart.setBackgroundColor(Color.parseColor("#CEC29B"));
                cityDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                streetDest.setBackgroundColor(Color.parseColor("#FCF7E6"));
                cityStart.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }
        });
    }


}

