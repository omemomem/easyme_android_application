package com.assistme.meirovichomer.easyme;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.network.HttpRequest;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class MainActivity extends AppCompatActivity {


    SharedPreferences firstRunPrefs = null;
    SharedPreferences screenFunction = null;

    public static final String PREFS_NAME = "first_run_config";

    public static final String PREFS_SCREEN = "screen_config";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        //Check if user runs the app for the first time.
        firstRunPrefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        screenFunction = getSharedPreferences(PREFS_SCREEN, MODE_PRIVATE);
        //Initialize ADS
        adMobIntilize();
        //Screen settings, Screen brightness and sleep mode.
        screenBrightnesMax();
        keepScreenAwake();
        checkPermissions();
        //Functions to open the other Activites
        openContacts();
        openCall();
        openHealth();
        openBanks();
        openMoovit();
        toAboutUs();
        //Function to close App.
        endActivity();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstRunPrefs.getBoolean(PREFS_NAME, true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            startActivity(new Intent(MainActivity.this, FirstRunConfig.class));
            firstRunPrefs.edit().putBoolean(PREFS_NAME, false).commit();
            finish();
        }
    }

    protected void adMobIntilize() {

        AdView mAdView;

        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/1921834349");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    // Button event to enter ContactActivity..
    protected void openContacts() {
        final Button btn = (Button) findViewById(R.id.toContacts_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ContactsActivity.class));
            }
        });
    }


    protected void openHealth() {
        Button btn = (Button) findViewById(R.id.toHealth_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, HealthActivity.class));
            }
        });
    }


    protected void openCall() {
        Button btn = (Button) findViewById(R.id.toCalls_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CallActivity.class));
            }
        });
    }


    protected void openBanks() {
        Button btn = (Button) findViewById(R.id.toBanks_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BanksActivity.class));
            }
        });
    }

    // Button event to exit from the app.
    protected void endActivity() {
        // The exit button to the MainActivity.
        Button btn = (Button) findViewById(R.id.closeApp_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    protected void openMoovit() {
        Button btn = (Button) findViewById(R.id.toMoovit_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MoovitActivity.class));
            }
        });
    }

    // Keeps screen awake, screen sleep is off,
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // sets the screen brightness to maximum plus sets current page (MainAcitivty) to max brightness
    protected void screenBrightnesMax() {

        String check = screenFunction.getString(PREFS_SCREEN, null);

        if (check == "true") {
            try {
                //sets manual mode and brightnes 255
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);  //this will set the manual mode (set the automatic mode off)
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 255);  //this will set the brightness to maximum (255)

                //refreshes the screen
                int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.screenBrightness = (float) br / 255;
                getWindow().setAttributes(lp);

            } catch (Exception e) {
                Toast.makeText(this, "Error has occured", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("SCREEN_CONFIG_DENY", "DENIED");
        }
    }

    protected void toAboutUs() {

        ImageView about = (ImageView) findViewById(R.id.aboutUsIV);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AboutUs.class));
            }
        });
    }

    protected void checkPermissions() {

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfWriteHas() ) {
                startActivity(new Intent(MainActivity.this, RequestPermissions.class));
                finish();
            }
            if (!checkIfFineLocHas()) {
                startActivity(new Intent(MainActivity.this, RequestPermissions.class));
                finish();
            }
            if (!checkIfCallPhoneHas()) {
                startActivity(new Intent(MainActivity.this, RequestPermissions.class));
                finish();
            }
        }

    }

    private boolean checkIfWriteHas() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfFineLocHas() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfCallPhoneHas() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


}




