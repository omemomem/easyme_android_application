package com.assistme.meirovichomer.easyme;

import android.Manifest;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class AddContact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        readContactPermission();
        //adMob
        adMobIntilize();
        isKeyboardUp();
        //Functions
        keepScreenAwake();
        endActivityEvent();
        buttonAddContactEvent();
        onEditTextsClick();
    }


    protected void readContactPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS}, 102);
    }


    // Function loads an ad.
    protected void adMobIntilize() {

        AdView mAdView;
        // ADMOB TEST BANNER AD: ca-app-pub-3940256099942544/6300978111
        MobileAds.initialize(this, "ca-app-pub-2474037887194071/7185978745");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    // Function ends the current activity.
    protected void endActivityEvent() {
        // The exit button to the MainActivity.
        Button endBtn = (Button) findViewById(R.id.backToContactBtn);
        endBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Function event when AddContact button is clicked.
    protected void buttonAddContactEvent() {
        Button contactAdd = (Button) findViewById(R.id.addContactPersonBtn);
        contactAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addContactAndCheck();
            }
        });
    }

    // Function checks if editTexts are not empty.
    protected void addContactAndCheck() {

        //Checks if the editTexts has anything written in them, If so, add to contacts else show the user what he needs.
        if (checkTextEditFields()) {
            addPersonToContactsFunction();
            toastAndFinish();
            toNoticeUser();

        } else {
            Toast.makeText(this, R.string.pleaseWrite_txt, Toast.LENGTH_SHORT).show();
            changeTextWhenWrong();
        }

    }



    // Function adds a contact to the user device.
    protected void addPersonToContactsFunction() {
        // Adds the contact, uses the value of the edittexts to get the name and numbers and then creates the contact.
        EditText tmpDisplayNumber = (EditText) findViewById(R.id.nameTxt);
        String displayName = tmpDisplayNumber.getText().toString();
        EditText tmpMobileNumber = (EditText) findViewById(R.id.cellphoneTxt);
        String mobileNumber = tmpMobileNumber.getText().toString();
        EditText tmpHomeNumber = (EditText) findViewById(R.id.phoneNumberTxt);
        String homeNumber = tmpHomeNumber.getText().toString();

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        //------------------------------------------------------ Names
        if (displayName.length() > 2) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            displayName).build());
        }

        //------------------------------------------------------ Mobile Number
        if (mobileNumber.length() > 2) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }

        //------------------------------------------------------ Home Numbers
        if (homeNumber.length() > 2) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, homeNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                    .build());
        }
        // Asking the Contact provider to create a new contact
        try {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            ops.clear();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    // Function changes the editTextHint to red.
    protected void changeTextWhenWrong() {

        // Changes editTexts color to red.

        EditText nameText = (EditText) findViewById(R.id.nameTxt);
        EditText phoneText = (EditText) findViewById(R.id.phoneNumberTxt);
        EditText cellPhoneText = (EditText) findViewById(R.id.cellphoneTxt);


        nameText.setHintTextColor(((Color.parseColor("#F10707"))));

        phoneText.setHintTextColor(((Color.parseColor("#F10707"))));

        cellPhoneText.setHintTextColor(((Color.parseColor("#F10707"))));


    }

    // Function finishes current activity and Toast.
    protected void toastAndFinish() {

        // Function will run after person was added, So the following message will display
        Toast.makeText(this, R.string.succsesfullyAddedContact_txt, Toast.LENGTH_LONG).show();
        // NOT SURE - The activity will finish in order to prevent confusion.
        finish();

    }

    // Check if the editTexts are empty.
    protected boolean checkTextEditFields() {

        // Function to check if editext are not empty.
        EditText tmpDisplayNumber = (EditText) findViewById(R.id.nameTxt);
        String displayName = tmpDisplayNumber.getText().toString();

        EditText tmpPhoneNumber = (EditText) findViewById(R.id.phoneNumberTxt);
        String phoneNum = tmpPhoneNumber.getText().toString();

        EditText tmpCellphoneNumber = (EditText) findViewById(R.id.cellphoneTxt);
        String cellPhoneNum = tmpCellphoneNumber.getText().toString();

        if (displayName.length() >= 1 && phoneNum.length() > 1 || displayName.length() >= 1 && cellPhoneNum.length() > 1) {
            return true;
        } else {
            return false;
        }

    }

    // Keep screen awake, meaning screen sleep is off.
    protected void keepScreenAwake() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    //When user adds a contact will send him to the NoticeActivity also adding sharedprefs to design the notice page.
    protected void toNoticeUser() {


        String PREFS_NAME = "notice_page";

        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("noticeMode", "addContact");
        editor.commit();

        startActivity(new Intent(AddContact.this, NoticeActivity.class));

    }

    // Function changes background of EditText when clicked.
    protected void onEditTextsClick() {

        final EditText nameText = (EditText) findViewById(R.id.nameTxt);
        final EditText phoneText = (EditText) findViewById(R.id.phoneNumberTxt);
        final EditText cellPhoneText = (EditText) findViewById(R.id.cellphoneTxt);


        // Name text onClick
        nameText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {


                // REGULAR: FCF7E6   DARKER: CEC29B
                nameText.setBackgroundColor(Color.parseColor("#CEC29B"));
                phoneText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                cellPhoneText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }

        });

        // Phone number onClick
        phoneText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                // REGULAR: FCF7E6   DARKER: CEC29B
                nameText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                phoneText.setBackgroundColor(Color.parseColor("#CEC29B"));
                cellPhoneText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                return false;
            }

        });


        // Phone number onClick
        cellPhoneText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                // REGULAR: FCF7E6   DARKER: CEC29B
                nameText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                phoneText.setBackgroundColor(Color.parseColor("#FCF7E6"));
                cellPhoneText.setBackgroundColor(Color.parseColor("#CEC29B"));
                return false;
            }
        });
    }


    // Function checks if keyboard is on, If so hides the bottom buttons and the adView
    protected void isKeyboardUp() {

        final View contentView = findViewById(android.R.id.content);

        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Button backButton = (Button) findViewById(R.id.backToContactBtn);
                AdView adView = (AdView) findViewById(R.id.adView);

                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("CheckHeight", "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    backButton.setVisibility(View.GONE);
                    adView.setVisibility(View.GONE);

                } else {
                    // keyboard is closed
                    backButton.setVisibility(View.VISIBLE);
                    adView.setVisibility(View.VISIBLE);
                }
            }
        });
    }


}
