/**
 * Created by meiro on 4/27/2017.
 */

package classes;

import java.util.HashSet;

public class SelectUserCall {
    private HashSet<String> email;
    private String phone;
    private HashSet<String> cellPhone;
    private String name;
    private boolean checked = false;


    public SelectUserCall() {
        this.email = new HashSet<String>();
        this.phone = "";
        this.cellPhone = new HashSet<String>();

    }

    public String getPhones() {
        return phone;
    }

    public void setPhone(String phone) {
        if (phone == null)
            return;
        this.phone = phone;
    }

    public String getPhone() {
        return phone.toString();
    }

    public String getCellPhone(){
      return cellPhone.toString();
    }
    public void setCellPhone(String cellPhone){

        if (cellPhone == null){
            return;
        }
        this.cellPhone.add(cellPhone.trim());
    }

    public void setEmail(String email) {
        if (email == null)
            return;
        this.email.add(email.trim());
    }

    public HashSet<String> getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}